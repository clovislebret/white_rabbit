<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        // The result ( � ?? �)
        $result = array( '1'=>0,'2'=>0,'5'=>0,'10'=>0,'20'=>0,'50'=>0,'100'=>0);
        // Dividable values
        $coinList = array(1,2,5,10,20,50,100);

        // Logic here is to go through the coin list and see how many time we can
        // divide the maximum coin value before dividing the next one. If we can we then calculate
        // the difference, add it to the result array, remove it from the initial amount and repeat.

        for($i=count($coinList)-1;$i>=0;$i--){
            if ($amount / $coinList[$i] >= 1 ) {
                $difference = floor($amount / $coinList[$i]);
                $result[(string)$coinList[$i]] = $difference;
                $amount -= ($coinList[$i] * $difference);
            }
        }
        return $result;
    }
}