<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        // Just parse and filter this bad boy
        return strtolower(preg_replace("/[^A-Za-z]/", '',file_get_contents($filePath)));
    }
    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        // Find occurrences of every letter
        $strArray = count_chars($parsedFile,1);
        // Sort the table by lowest values first
        asort($strArray);
        // Get the middle index
        $count = count($strArray);
        $middle = floor($count / 2);
        // Get the median letter
        $medianLetter = array_keys($strArray)[$middle-1];
        // Get its occurrences before return
        $occurrences = $strArray[$medianLetter];
        // Voila !
        return chr($medianLetter);

    }
}